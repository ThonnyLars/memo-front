import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppComponent } from './app.component';
import { FooterComponent } from './Footer/footer/footer.component';
import { HeaderComponent } from './Header/header/header.component';
import { MainBodyComponent } from './Body/main-body/main-body.component';
import { LoginComponent } from './Login/login/login.component';
import { InscriptionComponent } from './Login/inscription/inscription.component';
import { HomeComponent } from './Home/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { ModalModule} from 'ngx-bootstrap';
import { AdminProfilComponent } from './Login/Profiles/admin-profil/admin-profil.component';
import { UserProfilComponent } from './Login/Profiles/user-profil/user-profil.component';
import { DetailsComponent } from './Steps/details/details.component';
import { ListServices } from './Services/list.services';
import { listebyid } from './Services/list.by.id.services';
import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Authentification } from './Services/auth.services';
import { AccessDeniedComponent } from './Page-not-found/access-denied/access-denied.component';
import { SuccessInscriptionComponent } from './Login/success-inscription/success-inscription.component';
import { AddServices } from './Services/Add.services';
import { ConnexionDeniedComponent } from './Login/connexion-denied/connexion-denied.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    MainBodyComponent,
    LoginComponent,
    InscriptionComponent,
    HomeComponent,
    AdminProfilComponent,
    UserProfilComponent,
    DetailsComponent,
    AccessDeniedComponent,
    SuccessInscriptionComponent,
    ConnexionDeniedComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgxPaginationModule,
    HttpClientModule,
    ModalModule.forRoot()
  ],
  providers: [Authentification,CookieService,ListServices,listebyid,DatePipe,AddServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
