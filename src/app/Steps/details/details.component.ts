import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  final_url: string[] = [];
  numExemplaire = "";

  form: FormGroup;
  _com: FormControl = new FormControl("");

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
  };

  formins: FormGroup;
  inputEmail: FormControl = new FormControl("");
  inputPassword: FormControl = new FormControl("");

  constructor(private modalService: BsModalService, public cookieService: CookieService, private router: Router) {
    this.form = new FormGroup({
      _com: this._com
    });

    this.final_url = window.location.href.split('?')[1].split('=');

    /* Récupération du numero l'oeuvre*/
    this.numExemplaire = this.final_url[1].split('=')[0];
    //console.log(this.numExemplaire);

    this.formins = new FormGroup({
      inputEmail: this.inputEmail,
      inputPassword: this.inputPassword
    });
  }

  ngOnInit() {

  }

  /* Affichage des commentaires */
  Commentaire(commentaires: TemplateRef<any>) {
    this.modalRef = this.modalService.show(commentaires, this.config);
  }

  /* Publication d'un commentaire */
  Post(login) {

    console.log(this.form.value);

    //On vérifie si l'utilisateur est connecté
    if (this.cookieService.get('user') == "" || this.cookieService.get('user') == undefined) {
      this.modalRef = this.modalService.show(login, this.config);
      this.postIt();
    } else {
      this.postIt();
    }
  }

  /* Méthode pour ajout d'un commentaire */
  postIt() {

  }


  /* Réservation d'une oeuvre*/
  Reservation(login) {
    //On vérifie si l'utilisateur est connecté
    if (this.cookieService.get('user') == "" || this.cookieService.get('user') == undefined) {
      this.modalRef = this.modalService.show(login, this.config);

      this.validateReservation();
    } else {
      this.validateReservation();
    }
  }

  /* Méthode pour validation d'une réservation */
  validateReservation() {

  }

  /* Connexion */
  Validate(modalRef) {
    console.log(this.form.value);
    this.cookieService.set("user", JSON.stringify(this.form.value), 1, "/");
    modalRef.hide();
  }
}
