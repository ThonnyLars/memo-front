import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { User } from 'src/app/Model/Test';
import { Router } from '@angular/router';
import { Abonne } from 'src/app/Model/Abonne';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  //user: User = new User();

  abonne: Abonne = new Abonne();
  ok: boolean = false;
  constructor(public cookieService: CookieService, private router: Router) {
    //Si l'utilisateur est connecté
    if (this.cookieService.get('user') != "" && this.cookieService.get('user') != undefined) {
      //Pour affichage des boutons mon compte et se déconnecter
      this.ok = true;
    } else {
      //Pour affichage du bouton se connecter
      this.ok = false;
    }
  }

  ngOnInit() {
    if (this.cookieService.get('user') != "" && this.cookieService.get('user') != undefined) {
      this.abonne = JSON.parse(this.cookieService.get("user")) as Abonne;
    }
  }

  Deconnexion() {
    //Suppression des informations contenues dans les cookies
    this.cookieService.delete('user');
    this.router.navigateByUrl("/login");
   // window.location.reload();
  }

  Profil(){
    if(this.abonne.nom!="admin"){
      //console.log(this.abonne)
      this.router.navigateByUrl("/userProfil");
    }else{
        this.router.navigateByUrl("/adminProfil");
    }
  }

}
