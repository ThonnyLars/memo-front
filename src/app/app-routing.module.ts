import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Login/login/login.component';
import { InscriptionComponent } from './Login/inscription/inscription.component';
import { MainBodyComponent } from './Body/main-body/main-body.component';
import { HomeComponent } from './Home/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminProfilComponent } from './Login/Profiles/admin-profil/admin-profil.component';
import { UserProfilComponent } from './Login/Profiles/user-profil/user-profil.component';
import { DetailsComponent } from './Steps/details/details.component';
import { AccessDeniedComponent } from './Page-not-found/access-denied/access-denied.component';
import { SuccessInscriptionComponent } from './Login/success-inscription/success-inscription.component';
import { ConnexionDeniedComponent } from './Login/connexion-denied/connexion-denied.component';


const routes: Routes = [
  {path:"", component:HomeComponent},
  {path:"login",component:LoginComponent},
  {path:"inscription",component:InscriptionComponent},
  {path:"adminProfil", component:AdminProfilComponent},
  {path:"userProfil", component:UserProfilComponent},
  {path:"info", component:DetailsComponent},
  {path:"access_denied", component:AccessDeniedComponent},
  {path: "success", component:SuccessInscriptionComponent},
  {path:"connexion_denied", component: ConnexionDeniedComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule
  
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
