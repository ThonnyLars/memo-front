export class Abonne {
    id: number;
    numAbonne: string;
    cin: string;
    nom: string;
    prenom: string;
    adresse: string;
    email: string;
    tel: string;
    login: string;
    mdp: string;
}

export class Abonnement {
    id: number;
    numero: string;
    DateAbonnement: Date;
    DateFinAbonnementPrevu: Date;
    DateReelleFinAbonnement: Date;
    etat: boolean;
    abonne_id: number;
}