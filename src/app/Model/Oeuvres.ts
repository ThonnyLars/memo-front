export class Oeuvre {
    id: number;
    numeroISBN: string;
    titre: string;
    auteur: string;
    edition: string;
    DateParution: Date;
    Resume: string;
    QteTotale: number;
    QteDispo: number;
    image: string;
    categorie_id: number;
    section_id: number;
}

export class Categorie {
    id: number;
    libelle: string;
}

export class Exemplaire {
    id: number;
    numero: string;
    dateAcquisition: Date;
    oeuvre_id: number;
    abonnement_id: number;
}

export class Notes {
    id: number;
    NombreEtoile: number;
    abonne_id: number;
    oeuvre_id: number;
}

export class Commentaire {
    id: number;
    libelle: string;
    abonne_id: string;
    oeuvre_id: number;
}

export class Etat {
    id: number;
    libelle: string;
    dateEdition: Date;
    exemplaire_id: number;
}