export class Section{
    id :number;
    libelle:string;
}

export class Armoire{
    id :number;
    numero:string;
    nbreEtage:number;
}

export class Armoire_Section{
    section_id:number;
    armoire_id:number;
}