import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { User } from 'src/app/Model/Test';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Abonne, Abonnement } from 'src/app/Model/Abonne';
import { listebyid } from 'src/app/Services/list.by.id.services';
import { noUndefined } from '@angular/compiler/src/util';
import { Details } from 'src/app/Model/apiClass';
declare var $: any;

@Component({
  selector: 'app-user-profil',
  templateUrl: './user-profil.component.html',
  styleUrls: ['./user-profil.component.css']
})
export class UserProfilComponent implements OnInit {
  /* Pagination */
  p: number = 1;
  page: number = 1;

  profil: boolean = true;
  abonnements: boolean = false;
  oeuvres: boolean = false;

  form: FormGroup;
  _nom: FormControl = new FormControl("");
  _prenom: FormControl = new FormControl("");
  _tel: FormControl = new FormControl("");
  _adresse: FormControl = new FormControl("");
  _email: FormControl = new FormControl("");
  _pwd: FormControl = new FormControl("");
  _pwd_: FormControl = new FormControl("");
  _cin: FormControl = new FormControl("");
  _pseudo: FormControl = new FormControl("");

  /* Abonné ou Admin */
  abonne: Abonne = new Abonne();

  /* Liste des abonnements */
  AbonnementList: Abonnement[] = [];

  /* Etat des abonnements */
  state = "";

  /* Liste des Détails */
  detailListe: Details = new Details();

  constructor(private router: Router, private cookieService: CookieService, private ListeServiceById: listebyid) {

    $(document).on('click', '#div', function () {
      $(this).addClass('afterClick').siblings().removeClass('afterClick')
    })


    this.form = new FormGroup({
      _nom: this._nom,
      _prenom: this._prenom,
      _tel: this._tel,
      _adresse: this._adresse,
      _email: this._email,
      _pwd: this._pwd,
      _pwd_: this._pwd_,
      _cin: this._cin,
      _pseudo: this._pseudo
    });
  }

  ngOnInit() {
    /* Identification de l'utilisateur */
    /* Si l'utilisateur est connecté */
    if (this.cookieService.get('user') != "" && this.cookieService.get('user') != undefined) {
      this.abonne = JSON.parse(this.cookieService.get("user")) as Abonne;
    } else {
      /* Si l'utilisateur n'est pas connecté */
      this.router.navigateByUrl("/login");
    }

    //console.log(this.abonne)

    /* *********************** */
    this.profil = true;
    this.abonnements = false;
    this.oeuvres = false;
    this.showInformation();
  }

  /* Affichage des informations de l'abonné */
  showInformation() {
    this._nom = new FormControl(this.abonne.nom);
    this._cin = new FormControl(this.abonne.cin);
    this._prenom = new FormControl(this.abonne.prenom);
    this._tel = new FormControl(this.abonne.tel);
    this._email = new FormControl(this.abonne.email);
    this._adresse = new FormControl(this.abonne.adresse);
    this._pwd = new FormControl(this.abonne.mdp);
    this._pseudo = new FormControl(this.abonne.login);
    this._pwd_ = new FormControl("");
    this.form = new FormGroup({
      _nom: this._nom,
      _prenom: this._prenom,
      _tel: this._tel,
      _adresse: this._adresse,
      _email: this._email,
      _pwd: this._pwd,
      _pwd_: this._pwd_,
      _cin: this._cin,
      _pseudo: this._pseudo
    });

  }

  /* Affichage du menu de gestion des informations de l'utilisateur */
  show_profile() {
    this.profil = true;
    this.abonnements = false;
    this.oeuvres = false;
    this.showInformation();
  }

  /* Affichage du menu de consultation des abonnements */
  show_abonnement() {
    this.profil = false;
    this.abonnements = true;
    this.oeuvres = false;

    this.ListeServiceById.getUserAbo(this.abonne.id).subscribe((data: any) => {
      //console.log(data.data.Abonnement);
      if (data.data.Abonnement.length === 0) {

      } else {
        this.AbonnementList = data.data.Abonnement as Abonnement[];
        /*Chargement des détais*/

        console.log(this.AbonnementList)
        for (var i = 0; i < this.AbonnementList.length; i++) {
          this.ListeServiceById.getDetailsAbo(this.AbonnementList[i].id).subscribe((data1: any) => {
            //this.detailListe = new Details();
            this.detailListe = data1.data.Details as Details;
            console.log(this.detailListe.titre);
            console.log(this.detailListe);
          });
        }
      }
    });
  }

  /* Affichage du volet de consultation des oeuvres  */
  show_books() {
    this.profil = false;
    this.abonnements = false;
    this.oeuvres = true;
  }

  Validate() {
    //console.log(this.form.value);
  }
}
