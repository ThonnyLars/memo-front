import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { ListServices } from 'src/app/Services/list.services';
import { Abonne, Abonnement } from 'src/app/Model/Abonne';
import { Oeuvre, Categorie } from 'src/app/Model/Oeuvres';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { AddServices } from 'src/app/Services/Add.services';
import { listebyid } from 'src/app/Services/list.by.id.services';
import { Section } from 'src/app/Model/Section';
import { DatePipe } from '@angular/common';

/* declare var jquery : any; */
declare var $: any;

@Component({
  selector: 'app-admin-profil',
  templateUrl: './admin-profil.component.html',
  styleUrls: ['./admin-profil.component.css']
})
export class AdminProfilComponent implements OnInit {

  /* Conversion des dates */
  datePipe = new DatePipe('en-US');

  /* Chemin de l'image */
  image = "";

  /* Pagination */
  p: number = 1;
  page: number = 1;

  /* Actualisation */

  /* Pour affichage des rubriques */
  abonnement: boolean = true;
  oeuvres: boolean = false;
  users: boolean = false;
  demande: boolean = false;
  reservation: boolean = false;
  armoire: boolean = false;

  /* Formulaire d'ajout des utilisateurs */
  form: FormGroup;
  _nom: FormControl = new FormControl("");
  _prenom: FormControl = new FormControl("");
  _tel: FormControl = new FormControl("");
  _adresse: FormControl = new FormControl("");
  _email: FormControl = new FormControl("");
  _pwd: FormControl = new FormControl("");
  _pwd_: FormControl = new FormControl("");
  _cin: FormControl = new FormControl("");
  _pseudo: FormControl = new FormControl("");

  /* Formulaire d'ajout d'une oeuvre */
  formOeuvre: FormGroup;
  _isbn: FormControl = new FormControl("");
  _titre: FormControl = new FormControl("");
  _auteur: FormControl = new FormControl("");
  _edition: FormControl = new FormControl("");
  _parution: FormControl = new FormControl("");
  _resume: FormControl = new FormControl("");
  _qte: FormControl = new FormControl("");
  _image: FormControl = new FormControl("");
  _categorie: FormControl = new FormControl("");
  _section: FormControl = new FormControl("");

  /* Liste des abonnés */
  listAbonne: Abonne[] = [];

  /* Liste des oeuvres */
  listOeuvre: Oeuvre[] = [];

  /* Liste des demandes */
  listNonAbonne: Abonne[] = [];

  /* Liste des abonnements */
  listAbonnement: Abonnement[] = [];

  /* Liste des abonnements non retournés*/
  Abonnements_non_retourné: Abonnement[] = [];

  /* Abonné ou Admin */
  abonne: Abonne = new Abonne();

  /* Liste des catégories */
  ListeCategorie: Categorie[] = [];

  /* Liste des sections */
  ListeSection: Section[] = [];

  /* *************************** */
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
  };

  constructor(public datepipe: DatePipe,private addServices: AddServices, private router: Router, private cookieService: CookieService, private modalService: BsModalService, private ListeServices: ListServices, private listeById: listebyid) {
    $(document).on('click', '#div', function () {
      $(this).addClass('afterClick').siblings().removeClass('afterClick')
    })

    /* User */
    this.form = new FormGroup({
      _nom: this._nom,
      _prenom: this._prenom,
      _tel: this._tel,
      _adresse: this._adresse,
      _email: this._email,
      _pwd: this._pwd,
      _pwd_: this._pwd_,
      _cin: this._cin,
      _pseudo: this._pseudo
    });

    /* Oeuvre */
    this.formOeuvre = new FormGroup({
      _isbn: this._isbn,
      _titre: this._titre,
      _auteur: this._auteur,
      _edition: this._edition,
      _parution: this._parution,
      _resume: this._resume,
      _qte: this._qte,
      _image: this._image,
      _categorie: this._categorie,
      _section: this._section
    });
  }

  ngOnInit() {
    /* Identification de l'utilisateur */
    /* Si l'utilisateur est connecté */
    if (this.cookieService.get('user') != "" && this.cookieService.get('user') != undefined) {
      this.abonne = JSON.parse(this.cookieService.get("user")) as Abonne;

      /*Si l'utilisateur n'est pas admin*/
      if (this.abonne.nom != "admin") {
        console.log(this.abonne)
        this.router.navigateByUrl("/access_denied");
      }
    } else {
      /* Si l'utilisateur n'est pas connecté */
      this.router.navigateByUrl("/login");
    }

    /* *********************** */
    this.abonnement = true;
    this.oeuvres = false;
    this.users = false;
    this.demande = false;
    this.reservation = false;
    this.armoire = false;

    /* Chargement des liste des abonnements*/
    this.ListeServices.getAbonnementList().subscribe((data: any) => {
      this.listAbonnement = data.data as Abonnement[];
      console.log(data.data);
    });

    /* Liste des catégories */
    this.ListeServices.getCategorieList().subscribe((data: any) => {
      this.ListeCategorie = data.data as Categorie[];
      //console.log(data.data);
    });

  }

  /* ***************************************************** */
  /* ***************************************************** */

  /* Affichage du menu de gestion des armoires et sections */
  show_armoires() {
    this.armoire = true;
    this.abonnement = false;
    this.oeuvres = false;
    this.users = false;
    this.demande = false;
    this.reservation = false;
  }

  /* ***************************************************** */
  /* ***************************************************** */
  /* Affichage du menu de gestion des oeuvres */
  show_oeuvres() {
    this.abonnement = false;
    this.oeuvres = true;
    this.users = false;
    this.demande = false;
    this.reservation = false;
    this.armoire = false;

    /* Chargement des liste */
    this.ListeServices.getOeuvreList().subscribe((data: any) => {
      this.listOeuvre = data.data as Oeuvre[];
    });
  }

  /* ***************************************************** */
  /* ***************************************************** */
  /* Affichage du menu de gestion des demandes */
  show_demandes() {
    this.abonnement = false;
    this.oeuvres = false;
    this.users = false;
    this.demande = true;
    this.reservation = false;
    this.armoire = false;

    /* Chargement des liste */
    this.ListeServices.getDemandes().subscribe((data: any) => {
      this.listNonAbonne = data.data.Abonne as Abonne[];
    });
  }

  /* Validation de la demande */
  Accept(id) {
    this.addServices.Accept(id, "PT-000" + id).subscribe((data: any) => {
      if (data.data.ok === true) {
        /* Chargement des liste */

        this.listNonAbonne = [];
        this.ListeServices.getDemandes().subscribe((data: any) => {
          this.listNonAbonne = data.data.Abonne as Abonne[];
        });
      }
      window.location.reload();
    });
  }


  /* ***************************************************** */
  /* ***************************************************** */

  /* Affichage du menu de gestion des utilisateurs */
  show_users() {
    this.abonnement = false;
    this.oeuvres = false;
    this.users = true;
    this.demande = false;
    this.reservation = false;
    this.armoire = false;
    /* Chargement des liste */

    let list: Abonne[] = [];
    this.ListeServices.getAbonneList().subscribe((data: any) => {
      list = data.data as Abonne[];

      for (var i = 0; i < list.length; i++) {
        /* console.log(this.listAbonne[i].numAbonne); */
        if (list[i].numAbonne != 'none') {
          this.listAbonne.push(list[i]);
        }
      }
    });
  }

  /* ***************************************************** */
  /* ***************************************************** */

  /* Affichage du menu de gestion des réservations */
  show_reservations() {
    this.abonnement = false;
    this.oeuvres = false;
    this.users = false;
    this.demande = false;
    this.reservation = true;
    this.armoire = false;
  }

  /* ***************************************************** */
  /* ***************************************************** */

  /* Affichage du menu de gestion des abonnements */
  show_abonnements() {
    this.abonnement = true;
    this.oeuvres = false;
    this.users = false;
    this.demande = false;
    this.reservation = false;

    /* Chargement des liste */
    this.ListeServices.getAbonnementList().subscribe((data: any) => {
      this.listAbonnement = data.data as Abonnement[];
    });
  }

  /* ***************************************************** */
  /* ***************************************************** */

  /* Validation des informations du formulaire  d'ajout des utilisateurs*/
  Validate() {
    let abo: Abonne = new Abonne();

    abo.cin = this.form.value._cin;
    abo.nom = this.form.value._nom;
    abo.prenom = this.form.value._prenom;
    abo.adresse = this.form.value._adresse;
    abo.email = this.form.value._adresse;
    abo.tel = this.form.value._tel;
    abo.login = this.form.value._pseudo;
    abo.mdp = this.form.value._pwd_;

    this.addServices.Inscription(abo).subscribe((data: any) => {
      console.log(data);
      if (data.data.ok === true) {
        window.location.reload();
      }
    });

  }

  /* ***************************************************** */
  /* ***************************************************** */

  /* Validation des informations du formulaire  d'ajout des oeuvres*/
  Ajouter() {
    //console.log(this.formOeuvre.value._image);
    let o: Oeuvre = new Oeuvre();

    o.DateParution = this.formOeuvre.value._parution;
    o.numeroISBN = this.formOeuvre.value._isbn;
    o.titre = this.formOeuvre.value._titre;
    o.auteur = this.formOeuvre.value._auteur;
    o.QteTotale = this.formOeuvre.value._qte;
    o.Resume = this.formOeuvre.value._resume;
    o.categorie_id = this.formOeuvre.value._categorie;
    o.section_id = this.formOeuvre.value._section;
    o.edition = this.formOeuvre.value._edition;
    o.image = this.image;

    this.datepipe.transform(o.DateParution, 'yyyy-MM-dd');
    console.log(o.DateParution);
    this.addServices.AddOeuvre(o).subscribe((data: any) => {
      if (data.data.ok === true) {
        window.location.reload();
      }
    });
  }

  GetImage(event) {
    this.image = event.target.files[0].name;
  }

  /* ***************************************************** */
  /* ***************************************************** */

  /* Affichage du formulaire d'ajout des utilisatuers */
  AddUser(user: TemplateRef<any>) {
    this.modalRef = this.modalService.show(user, this.config);
  }

  /* ***************************************************** */
  /* ***************************************************** */

  /* Affichage du formulaire d'ajout d'oeuvre */
  AddOeuvre(oeuvre: TemplateRef<any>) {
    this.modalRef = this.modalService.show(oeuvre, this.config);

  }

  /* ***************************************************** */
  /* ***************************************************** */


  /* Recherche d'une section en fonction de la catégorie */
  FindSection() {
    for (var i = 0; i < this.ListeCategorie.length; i++) {
      if (this.ListeCategorie[i].id == this.formOeuvre.value._categorie) {
        /* Chargement des liste */
        this.listeById.getSectionByCategorie(this.ListeCategorie[i].libelle).subscribe((data: any) => {
          this.ListeSection = data.data.Section as Section[];

        });
        break;
      }
    }
  }

}
