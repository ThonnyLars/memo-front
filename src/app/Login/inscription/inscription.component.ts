import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Abonne } from 'src/app/Model/Abonne';
import { Router } from '@angular/router';
import { Authentification } from 'src/app/Services/auth.services';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {
  /* Désactiver le boutton */
 ok = false;

  form: FormGroup;
  _nom: FormControl = new FormControl("");
  _prenom: FormControl = new FormControl("");
  _tel: FormControl = new FormControl("");
  _adresse: FormControl = new FormControl("");
  _email: FormControl = new FormControl("");
  _pwd: FormControl = new FormControl("");
  _pwd_: FormControl = new FormControl("");
  _cin: FormControl = new FormControl("");
  _pseudo: FormControl = new FormControl("");

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
  };

  /* Déclaration d'un abonne */
  abo: Abonne = new Abonne();
  constructor(private modalService: BsModalService, private router: Router, private auth: Authentification) {
    this.form = new FormGroup({
      _nom: this._nom,
      _prenom: this._prenom,
      _tel: this._tel,
      _adresse: this._adresse,
      _email: this._email,
      _pwd: this._pwd,
      _pwd_: this._pwd_,
      _cin: this._cin,
      _pseudo: this._pseudo
    });
  }

  ngOnInit() {
  }

  Validate(erreur: TemplateRef<any>) {
    //console.log(this.form.value);
    this.ok = true;

    this.abo.numAbonne = "none";
    this.abo.cin = this.form.value._cin;
    this.abo.nom = this.form.value._nom;
    this.abo.prenom = this.form.value._prenom;
    this.abo.adresse = this.form.value._adresse;
    this.abo.email = this.form.value._adresse;
    this.abo.tel = this.form.value._tel;
    this.abo.login = this.form.value._pseudo;
    this.abo.mdp = this.form.value._pwd_;

    this.auth.Inscription(this.abo).subscribe((data: any) => {
      console.log(data);
      if (data.data.ok === true) {
        this.router.navigateByUrl("/success");
      } else {
        this.ok = false;
        this.modalRef = this.modalService.show(erreur, this.config);
      }
    });
    //console.log(this.abo);
  }

}
