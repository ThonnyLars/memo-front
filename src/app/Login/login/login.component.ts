import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { Abonne } from 'src/app/Model/Abonne';
import { Authentification } from 'src/app/Services/auth.services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  inputEmail: FormControl = new FormControl("");
  inputPassword: FormControl = new FormControl("");
  error = false;
  constructor(public cookieService: CookieService, private router: Router, private auth: Authentification) {

    //Redirection de l'utilisateur vers la page d'acceuil s'il est déjà connecté 
    if (this.cookieService.get('user') != "" && this.cookieService.get('user') != undefined) {
      this.router.navigateByUrl("/");
    }
    this.form = new FormGroup({
      inputEmail: this.inputEmail,
      inputPassword: this.inputPassword
    });
  }

  ngOnInit() {
  }

  Validate() {
    let abonne: Abonne;
    abonne = new Abonne();
    abonne.login = this.form.value.inputEmail;
    abonne.mdp = this.form.value.inputPassword;
    console.log(abonne);

    //Connexion
    this.auth.LogIn(abonne).subscribe((data: any) => {
      console.log(data);
      if (data.data.Abonne === null || data.data.Abonne === undefined) {
        this.error = true;
      } else {
        abonne = data.data.Abonne;
        console.log(abonne);
        if (abonne.numAbonne === "none") {
          this.router.navigateByUrl("/connexion_denied");
        } else {
          this.cookieService.set("user", JSON.stringify(abonne), 1, "/");
          this.router.navigateByUrl("/");
        }
      }
    });



  }



}
