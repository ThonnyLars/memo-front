import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnexionDeniedComponent } from './connexion-denied.component';

describe('ConnexionDeniedComponent', () => {
  let component: ConnexionDeniedComponent;
  let fixture: ComponentFixture<ConnexionDeniedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnexionDeniedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnexionDeniedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
