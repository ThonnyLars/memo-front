import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-connexion-denied',
  templateUrl: './connexion-denied.component.html',
  styleUrls: ['./connexion-denied.component.css']
})
export class ConnexionDeniedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
