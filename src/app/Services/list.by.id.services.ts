import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Observable, throwError, from } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Section } from '../Model/Section';
import { Categorie, Commentaire } from '../Model/Oeuvres';

@Injectable({
    providedIn: 'root'
})


export class listebyid {

    private httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        // 'Authorization': 'my-auth-token'
    });

    private httpParams = new HttpParams();
    base_url = "http://127.0.0.1:8000/api/"
    constructor(private http: HttpClient, private datepipe: DatePipe) {

    }

    /* ****************************************************************************** */

    /* 
        * Section by id
    */
    findBySectionById(id: number): Observable<Section> {
        return this.http.get<Section>(this.base_url + "sections/" + id).pipe(
            catchError(this.handleError)
        );
    }


    /* 
         * Liste des Commentaires
   */
    getCommentaireList(num: string): Observable<Commentaire[]> {
        //this.base_url = "https://bip-module.herokuapp.com/api/";
        return this.http.get<Commentaire[]>(this.base_url + "commentaires/" + num).pipe(
            catchError(this.handleError)
        );
    }

    /* 
         * Abonnements par user
   */
    getUserAbo(id: number): Observable<any> {
        console.log(this.base_url + "abonnements/getById?id=" + id)
        return this.http.post<any>(this.base_url + "abonnements/getById?id=" + id, null).pipe(
            catchError(this.handleError)
        );
    }


    /* 
         * Détails
   */
    getDetailsAbo(id: number): Observable<any> {
        console.log(this.base_url + "abonnements/getByUser?id=" + id)
        return this.http.post<any>(this.base_url + "abonnements/getByUser?id=" + id, null).pipe(
            catchError(this.handleError)
        );
    }



    /* ****************************************************************************** */

    /* 
         * Détails
   */
    getSectionByCategorie(categorie: string): Observable<any> {
        console.log(this.base_url + "Section/categories?categorie=" + categorie)
        return this.http.post<any>(this.base_url + "Section/categories?categorie=" + categorie, null).pipe(
            catchError(this.handleError)
        );
    }



    /* ****************************************************************************** */


    /* 
      * Gestion des erreurs 
    */
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }

}