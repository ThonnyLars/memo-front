import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Observable, throwError, from } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Section } from '../Model/Section';
import { Categorie, Commentaire, Oeuvre } from '../Model/Oeuvres';
import { Abonne } from '../Model/Abonne';
import { DateFormatter } from 'ngx-bootstrap';

@Injectable({
    providedIn: 'root'
})


export class AddServices {

    //private datepipe: DatePipe;

    private httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        // 'Authorization': 'my-auth-token'
    });

    private httpParams = new HttpParams();
    base_url = "http://127.0.0.1:8000/api/"
    constructor(private http: HttpClient, private datepipe: DatePipe) {

    }

    /* ****************************************************************************** */

    /* 
      * Validation de la demande d'adhésion 
    */
    Accept(id: number, numAbonne): Observable<any> {
        return this.http.post<any>(this.base_url + "demandes/accept?numAbonne=" + numAbonne
            + "&id=" + id + "", null).pipe(catchError(this.handleError)
            );

    }




    /* ****************************************************************************** */

    /* Inscription */
    Inscription(abonne: Abonne): Observable<any> {
        return this.http.post<any>(this.base_url + "demandes/admin/add?"
            + "&cin=" + abonne.cin + "&nom=" + abonne.nom + "&prenom=" + abonne.prenom + "&adresse=" + abonne.adresse
            + "&email=" + abonne.email + "&tel=" + abonne.tel + "&login=" + abonne.login + "&mdp=" + abonne.mdp + "",
            null).pipe(catchError(this.handleError)
            );

    }

    /* Inscription */
    AddOeuvre(o: Oeuvre): Observable<any> {
        //numeroISBN=bbbbbbbbb&titre=kkkkkk&auteur=bnn,&edition=llll
        //&Resume=oooo&QteTotale=2&image=hhh&categorie_id=1&section_id=1
        
        console.log(this.datepipe.transform(new Date(Date.now()), 'yyyy-MM-dd'));
        console.log(this.base_url + "Oeuvre/addAdmin?"
        + "numeroISBN=" + o.numeroISBN + "&titre=" + o.titre + "&auteur=" + o.auteur + "&edition=" + o.edition
        + "&Resume=" + o.Resume + "&QteTotale=" + o.QteTotale + "&image=" + o.image + "&categorie_id=" + o.categorie_id+
        "&section_id="+o.section_id+"&dateAcquisition="+this.datepipe.transform(new Date(Date.now()), 'yyyy-MM-dd')+"&DateParution="+o.DateParution)


        return this.http.post<any>(this.base_url + "Oeuvre/addAdmin?"
        + "numeroISBN=" + o.numeroISBN + "&titre=" + o.titre + "&auteur=" + o.auteur + "&edition=" + o.edition
        + "&Resume=" + o.Resume + "&QteTotale=" + o.QteTotale + "&image=" + o.image + "&categorie_id=" + o.categorie_id+
        "&section_id="+o.section_id+"&dateAcquisition="+this.datepipe.transform(new Date(Date.now()), 'yyyy-MM-dd')+"&DateParution="+o.DateParution,
            null).pipe(catchError(this.handleError)
            );

    }

    /* ****************************************************************************** */


    /* 
      * Gestion des erreurs 
    */
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }

}