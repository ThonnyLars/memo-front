import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Observable, throwError, from } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Section } from '../Model/Section';
import { Categorie, Commentaire } from '../Model/Oeuvres';
import { Abonne } from '../Model/Abonne';

@Injectable({
    providedIn: 'root'
})


export class Authentification {

    private httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        // 'Authorization': 'my-auth-token'
    });

    private httpParams = new HttpParams();
    base_url = "http://127.0.0.1:8000/api/"
    constructor(private http: HttpClient, private datepipe: DatePipe) {

    }

    /* Connexion */
    LogIn(abonne: Abonne): Observable<any> {
        //this.base_url="https://bip-module.herokuapp.com/api/";
        console.log(abonne);

        //console.log(this.base_url+"connexion/login?login="+abonne.login+"&pw="+abonne.mdp+"");
        return this.http.post<any>(this.base_url + "connexion/login?login=" + abonne.login + "&mdp=" + abonne.mdp + "", null).pipe(
            catchError(this.handleError)
        );

    }

    /* Inscription */
    Inscription(abonne: Abonne): Observable<any> {
        return this.http.post<any>(this.base_url + "demandes/add?numAbonne=" + abonne.numAbonne
            + "&cin=" + abonne.cin + "&nom=" + abonne.nom + "&prenom=" + abonne.prenom + "&adresse=" + abonne.adresse
            + "&email=" + abonne.email + "&tel=" + abonne.tel + "&login=" + abonne.login + "&mdp=" + abonne.mdp + "",
            null).pipe(catchError(this.handleError)
            );

    }


    /* ****************************************************************************** */

    /* 
      * Gestion des erreurs 
    */
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }

}