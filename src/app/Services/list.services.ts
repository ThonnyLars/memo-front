import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Observable, throwError, from } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Section, Armoire } from '../Model/Section';
import { Categorie, Oeuvre, Commentaire } from '../Model/Oeuvres';
import { Abonne, Abonnement } from '../Model/Abonne';

@Injectable({
    providedIn: 'root'
})

export class ListServices {

    private httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        // 'Authorization': 'my-auth-token'
    });

    private httpParams = new HttpParams();
    base_url = "http://127.0.0.1:8000/api/"
    constructor(private http: HttpClient, private datepipe: DatePipe) {

    }

    /* ****************************************************************************** */

    /* 
        * Liste des Sections
    */
    getSectionList(): Observable<any> {

        return this.http.get<any>(this.base_url + "sections/").pipe(
            catchError(this.handleError)
        );
    }

    /* ****************************************************************************** */

    /* 
          * Liste des Catégories
    */
    getCategorieList(): Observable<any> {

        return this.http.get<any>(this.base_url + "categories/").pipe(
            catchError(this.handleError)
        );
    }


    /* ****************************************************************************** */

    /* 
          * Liste des Abonnés
    */
    getAbonneList(): Observable<any> {

        return this.http.get<any>(this.base_url + "abonnes/").pipe(
            catchError(this.handleError)
        );
    }

    /* ****************************************************************************** */

    /* 
          * Liste des Abonnements
    */
    getAbonnementList(): Observable<any> {

        return this.http.get<any>(this.base_url + "abonnements/").pipe(
            catchError(this.handleError)
        );
    }


    /* ****************************************************************************** */

    /* 
          * Liste des oeuvres
    */
    getOeuvreList(): Observable<any> {

        return this.http.get<any>(this.base_url + "oeuvres/").pipe(
            catchError(this.handleError)
        );
    }

    /* 
          * Liste des Armoires
    */
    getArmoireList(): Observable<any> {

        return this.http.get<any>(this.base_url + "armoires/").pipe(
            catchError(this.handleError)
        );
    }


    /* ****************************************************************************** */


    /* 
          * Liste des Armoires
    */
    getDemandes(): Observable<any> {
        return this.http.get<any>(this.base_url + "demandes/list").pipe(
            catchError(this.handleError)
        );
    }


    /* ****************************************************************************** */


    /* 
      * Gestion des erreurs 
    */
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }
}